package;

import luxe.Component;
import luxe.Entity;
import luxe.Sprite;

import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;

import luxe.Vector;

enum Position {
	right;
	left;
}

class WalkingAlien
{
	private var orient : Int = 1;
	private var max_left : Int = -250;
	private var max_right : Int = 80;
	private var speed : Float = 200;
	private var toPush : Bool = true;
	private var position : Position = Position.left;
	
	public var body : Body;
	public var sprite : Sprite;
	
	public var onReadyToPush : Dynamic;
	
	public function new(maxleft, maxright, y, pos : Position) 
	{
		max_left = maxleft;
		max_right = maxright;
		position = pos;
		
		var xpos = max_left;
		if (pos == Position.right) {
			orient = -1;
			xpos = max_right; 
		} else {
			orient = 1;
		}
		
		sprite = new Sprite( {
			name : "actor1",
			texture : Luxe.loadTexture("assets/alien.png"),
			pos : new Vector(xpos, y),
			size : new Vector(60, 60),
			depth : 1
		});
		
		body = new Body(BodyType.DYNAMIC);
		body.shapes.add(new Polygon(Polygon.box(60, 60)));
		body.position.setxy(xpos, y);
		body.space = Luxe.physics.nape.space;
		
	}
	
	public function setSpeed(s:Float) 
	{
		speed = s;
	}
	
	public function update(dt:Float) 
	{		
		sprite.pos.y = body.position.y;
		sprite.pos.x = body.position.x;
		sprite.rotation.setFromEuler(new Vector(0, 0, body.rotation));
					
		if (body.position.x > max_right) {
			if (position == Position.left) {
				toPush = true;
			} else {
				if ((onReadyToPush != null) && (toPush)) {
					onReadyToPush();
					toPush = false;
				}
			}
			
			orient = -1;			
		}
		
		if (body.position.x < max_left) {			
			if (position == Position.left) {
				if ((onReadyToPush != null) && (toPush)) {
					onReadyToPush();
					toPush = false;
				}
			} else {
				toPush = true;				
			}
						
			orient = 1;
		}		
					
		body.position.x += orient * speed * dt;		
	}
	
}