package;

import luxe.Component;
import luxe.Sprite;
import nape.constraint.Constraint;
import nape.phys.Body;
import luxe.Vector;
import luxe.tween.Actuate;
import luxe.components.sprite.SpriteAnimation;

import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;
import nape.shape.Circle;

class BoxSprite extends Sprite {
	public var body : Body;
	public var isToggled : Bool = false;
	public var customData : Dynamic;
	public var joint : Constraint;
	
	private var isExploding : Bool = false;
	private var explodeCountdown : Float = 0.0;
	private var explodeTime : Float = 1.2;	
	public var onExplodeStart: Dynamic;
	
	public function new(asset:String, ps:Vector, custom:Dynamic) 
	{
		super({			
			texture : Luxe.loadTexture(asset),
			pos : ps,
			size : new Vector(60, 60),
			depth : 1
		});
		
		customData = custom;
		
		body = new Body(BodyType.DYNAMIC);
		body.shapes.add(new Polygon(Polygon.box(60, 60)));
		body.position.setxy(ps.x, ps.y);
		body.space = Luxe.physics.nape.space;
	}
	
	public function destroyBox() {				
		if (joint != null) Luxe.physics.nape.space.constraints.remove(joint);
		Luxe.physics.nape.space.bodies.remove(body);
		destroy();
	}
	
	public function toggle() {		
		isToggled = !isToggled;
		
		if (isToggled) {					
			Actuate.tween(this.color, 0.7, { a : 0.4, g : 0.2, b: 0.2 } ).repeat().reflect();
		} else {
			Actuate.stop(this.color);
			this.color.a = 1.0;
			this.color.g = 1.0;
			this.color.b = 1.0;
		}
	}
	
	public function explode() {
		isExploding = true;			
	}
	
	private function animateExplode() {
		var anim_object = Luxe.loadJSON('assets/explosion.json');
		
		var explosion = new Sprite({			
			centered : true,
			texture : Luxe.loadTexture("assets/explosion.png"),
			pos : this.pos,
			size : new Vector(60, 60),
			depth : 1
		});			
		
		var anim = explosion.add(new SpriteAnimation( { name:'explode' } ));
		anim.add_from_json_object( anim_object.json );
		anim.animation = 'explode';					
		
		explosion.events.listen('complete', function(_) {
			Actuate.stop(this.color);
			this.color.a = 1.0;
			this.color.g = 1.0;
			this.color.b = 1.0;
			explosion.destroy();			
		});
		
		anim.play();			
		if (onExplodeStart != null) onExplodeStart();
		Luxe.camera.shake( 100 );		
		destroyBox();		
	}
	
	override function update(dt:Float) {		
		this.pos.y = body.position.y;
		this.pos.x = body.position.x;
		this.rotation.setFromEuler(new Vector(0, 0, body.rotation));		
		
		if (isExploding) {
			explodeCountdown += dt;
			var xs = Math.random() > 0.5 ? 1 : -1;
			var ys = Math.random() > 0.5 ? 1 : -1;			
		
			this.pos.x = this.pos.x + 1 * Math.random() * xs;
			this.pos.y = this.pos.y + 1 * Math.random() * ys;
			
			if (explodeCountdown > explodeTime) {				
				animateExplode();				
			}
		}
	}
}