package;

import luxe.Input;
import luxe.Sprite;
import luxe.States;
import luxe.Vector;
import luxe.Parcel;
import luxe.Color;
import luxe.ParcelProgress;
import phoenix.Texture;
using StringTools;

typedef Letter = {
	var name : String;
	var asset : String;
}

class Main extends luxe.Game 
{
	public static var states : States;
	public static var letters = new Array<Letter>();
	public static var dictionary = new Map<String, Int>();
	public static var scores : Int = 0;
	
	private function fillLetters() {
		//letters.push( { name : "a", asset : "assets/boxA.png" } );
		letters.push( { name : "б", asset : "assets/boxB.png" } );		
		letters.push( { name : "к", asset : "assets/boxK.png" } );
		letters.push( { name : "и", asset : "assets/boxI.png" } );
		letters.push( { name : "у", asset : "assets/boxU.png" } );
	}
	
	private function fillDictionary() {
		Luxe.loadText("assets/word_rus.txt", function(e) {
			var its = e.text.split('\n');
			for (it in its) {
				dictionary[it.trim()] = 0;
			}
			
		}, true);
	}
	
	override function ready()
	{		
		Luxe.physics.nape.draw = false;		
		
		fillLetters();
		fillDictionary();
		
		var json_asset = Luxe.loadJSON('assets/parcel.json');
		
		var preloader = new Parcel();
		preloader.from_json(json_asset.json);
		
		var progress = new ParcelProgress({
            parcel      : preloader,
            background  : new Color(255,255,255,1),
            oncomplete  : onloaded
        });

        preloader.load();
	}
	
	function onloaded(_) {		
		states = new States( { name : 'states' } );
		states.add(new MenuState());
		states.add(new GameState());
		states.set('game');
	}
	
	override function config( config:luxe.AppConfig )
	{
        //config.render.antialiasing = 8;
        return config;
    }

	override function onkeyup(e:KeyEvent) 
	{
		if (e.keycode == Key.ac_back) Luxe.shutdown();
		
		if(e.keycode == Key.escape)
			Luxe.shutdown();
	}

	override function update(dt:Float) 
	{
	}
}
