package;

import luxe.Sprite;
import luxe.States;
import luxe.tween.easing.Sine;
import luxe.Vector;
import phoenix.Texture;
import snow.utils.Timer;
import luxe.tween.Actuate;
import luxe.Input.MouseEvent;
import luxe.Text;
import luxe.Color;

import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;
import nape.shape.Circle;
import WalkingAlien;
import Main;

class GameState extends State
{
	private var border : Body;
	private var gameBack : Sprite;
	private var one : Sprite;
	private var two : Sprite;
	private var three : Sprite;
	private var wordText : Text;
	private var scoreText : Text;
	private var isReady : Bool = false;
	
	private var alienLeft : WalkingAlien;
	private var alienRight : WalkingAlien;	

	private var boxes = new Array<BoxSprite>();
	private var selBoxes = new Array<BoxSprite>();
	private var isExploding : Bool = false;
	
	public function new() 
	{
		super( { name:'game' } );
	}
	
	private function newBox(pos : Position) {
		var letter : Letter = Main.letters[Math.round(Math.random() * (Main.letters.length - 1))];		

		var ps : Vector = null;
		if (pos == Position.left) {			
			ps = new Vector(-30, 70);
		} else if (pos == Position.right) {
			ps = new Vector(510, 70);
		}
		
		var bs = new BoxSprite(letter.asset, ps, letter);
		boxes.push(bs);
	}
	
	private function createActors() 
	{
		alienLeft = new WalkingAlien( -250, 70, 70, Position.left);
		
		newBox(Position.left);
		alienLeft.onReadyToPush = function() {
			newBox(Position.left);
		}
		
		alienRight = new WalkingAlien(410, 730, 70, Position.right);
		
		newBox(Position.right);
		alienRight.onReadyToPush = function() {
			newBox(Position.right);
		}
	}
	
	private function createCountDown() 
	{							
		var w = Luxe.screen.w;
        var h = Luxe.screen.h;
		
		one = new Sprite( {
			name : 'one',
			centered : true,
			texture : Luxe.loadTexture("assets/one.png"),
			pos : new Vector(w/2, 300),
			size : new Vector(180, 180),
			depth : -1,
			scale : new Vector(0,0)
		});		
		
		two = new Sprite( {
			name : 'two',
			centered : true,
			texture : Luxe.loadTexture("assets/two.png"),
			pos : new Vector(w/2, 300),
			size : new Vector(180, 180),
			depth : -1,
			scale : new Vector(0,0)
		});
		
		three = new Sprite({
			name : 'three',
			centered : true,
			texture : Luxe.loadTexture("assets/three.png"),
			pos : new Vector(w/2, 300),
			size : new Vector(180, 180),
			depth : -1,
			scale : new Vector(0,0)
		});
		
		Actuate.tween(one.scale, 0.5, { x : 1, y : 1 } ).onComplete(function() {
			Actuate.tween(one.scale, 0.6, { x : 0, y : 0 } ).onComplete(function() {
				Actuate.tween(two.scale, 0.5, { x : 1, y : 1 } ).onComplete(function() {
					Actuate.tween(two.scale, 0.6, { x : 0, y : 0 } ).onComplete(function() {
						Actuate.tween(three.scale, 0.5, { x : 1, y : 1 } ).onComplete(function() {
							Actuate.tween(three.scale, 0.6, { x : 0, y : 0 } ).onComplete(function() {
								
							});
						});
					});
				});
			});
		});				
	}
	
	private function createBackground() {
		var w = Luxe.screen.w;
        var h = Luxe.screen.h;
		
		gameBack = new Sprite( {
			name : 'gameBack',
			centered : false,
			texture : Luxe.loadTexture("assets/gamebackground.png"),
			pos : new Vector(0, 0),
			size : new Vector(w, h),
			depth : -1
		});
		
		var font = Luxe.loadFont("assets/mico.fnt");
		font.onload = function(_) {
			wordText = new Text({
                text: "",
                pos : new Vector(Luxe.screen.mid.x, 0),
                point_size : 40,
                font: font,
				align: TextAlign.center,
				letter_spacing : 2,
				color: new Color(1,1,0.1, 1)
            });
			
			scoreText = new Text({
                text: "0",				
                pos : new Vector(Luxe.screen.w - 10, 0),
                point_size : 40,
                font: font,
				align: TextAlign.right,
				color: new Color(1,0.8,0.3, 1)
            });
		};
	}
	
	private function createWalls() 
	{
		var w = Luxe.screen.w;
        var h = Luxe.screen.h;
		
	    border = new Body(BodyType.STATIC);				
		border.shapes.add(new Polygon(Polygon.rect(-300, 100, 400, 30)));
		border.shapes.add(new Polygon(Polygon.rect(0, 100, 30, 700)));
		border.shapes.add(new Polygon(Polygon.rect(0, 765, 480, 30)));
		border.shapes.add(new Polygon(Polygon.rect(450, 100, 30, 700)));
		border.shapes.add(new Polygon(Polygon.rect(380, 100, 400, 30)));
		border.space = Luxe.physics.nape.space;
	}
	
	private function showStars() {
		var texture = Luxe.loadTexture("assets/star.png");
		
		for (en in selBoxes) {
			var sprite = new Sprite({
				centered : true,
				texture : texture,
				pos : en.pos,
				size : new Vector(40, 40),
				depth : 1
			});
			
			Main.scores += 10;
						
			Actuate.tween(sprite.pos, 1.5, { x : scoreText.pos.x - 10, y : scoreText.pos.y + 20 } ).ease(Sine.easeIn).onComplete(function() {				
				sprite.destroy();
				scoreText.text = Std.string(Main.scores);
				Actuate.tween(scoreText.scale, 0.5, { x : 1.2, y : 1.2 } ).reverse();				
			});
		}
	}
	
	private function checkWord() {
		var sb = new StringBuf();
		for (en in selBoxes) {
			sb.add(en.customData.name);
		}
		wordText.text = sb.toString();
		
		if (Main.dictionary.exists(sb.toString())) {
			for (en in selBoxes) {
				en.explode();
			}
			selBoxes[0].onExplodeStart = function() {				
				wordText.text = "";
				isExploding = false;
				showStars();
				selBoxes = new Array<BoxSprite>();
			};			
			isExploding = true;
		}		
	}
	
	private function toggleBox(e:BoxSprite) 
	{
		if (e.isToggled) {
			selBoxes.remove(e);
		} else {
			selBoxes.push(e);			
		}
		e.toggle();		
		checkWord();
	}
	
	override public function onenter<T>(d:T) 
	{		
		createBackground();	
		createWalls();
		createActors();
		
		isReady = true;
	}	
	
	override public function onleave<T>(d:T)
	{
		
	}
	
	override public function onmouseup(e:MouseEvent) 
	{
		if (!isReady) return;
		if (isExploding) return;
		
		for (en in boxes) {
			if (en.point_inside(e.pos)) {								
				toggleBox(en);
			}
		}
	}
	
	override public function update(dt:Float)
	{	
		alienLeft.update(dt);
		alienRight.update(dt);
	}
}