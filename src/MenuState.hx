package;

import luxe.tween.Actuate;
import luxe.Input.MouseEvent;
import luxe.Sprite;
import luxe.States.State;
import luxe.Vector;
import nape.constraint.DistanceJoint;
import snow.utils.Timer;

import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;
import nape.shape.Circle;
import nape.constraint.PivotJoint;

class MenuState extends State
{
	private var menuBack : Sprite;
	private var startButton : Sprite;
	private var scoreButton : Sprite;
	private var soundSwitch : Sprite;
	private var exitButton : Sprite;
	private var bestTemp : Sprite;
	
	private var border : Body;
	private var boxes : Array<BoxSprite> = new Array<BoxSprite>();
	
	private var isReady : Bool = false;
	
	public function new() 
	{
		super( { name:'menu' } );
				
		Luxe.audio.create('assets/menumusic.ogg', 'menumusic', true);
		Luxe.audio.create('assets/slide.ogg', 'slide', true);
		Luxe.audio.create('assets/click.ogg', 'click', true);			
	}
	
	function newBox(n, x, y, texture, d) 
	{
		/*var box = new Sprite( {
			name : n,			
			texture : Luxe.loadTexture(texture),
			pos : new Vector(0,0),
			size : new Vector(60, 60),
			depth : 1
		});			
					
		var body = new Body(BodyType.DYNAMIC);
		body.shapes.add(new Polygon(Polygon.box(60, 60)));
		body.position.setxy(x, y);
		body.space = Luxe.physics.nape.space;
		
		var distJoint = new DistanceJoint(border, body, Vec2.weak(x-10, 5), Vec2.weak(0, -30), d, d+40 );
		distJoint.space = Luxe.physics.nape.space;				
		var bs = new BoxSprite(body, box);
		bs.joint = distJoint;
		boxes.push(bs);*/
	}
	
	override public function onenter<T>(d:T)
	{
		var w = Luxe.screen.w;
        var h = Luxe.screen.h;
		
		menuBack = new Sprite( {
			name : 'menuBack',
			centered : false,
			texture : Luxe.loadTexture("assets/menubackground.png"),
			pos : new Vector(0, 0),
			size : new Vector(w, h),
			depth : -1
		});
		
		startButton = new Sprite( {
			name : 'startButton',
			centered : false,			
			texture : Luxe.loadTexture("assets/startbutton.png"),
			pos : new Vector(-300, 280),
			size : new Vector(300, 87),
			depth : 1
		});
		
		scoreButton = new Sprite( {
			name : 'scoreButton',
			centered : false,
			texture : Luxe.loadTexture("assets/scorebutton.png"),
			pos : new Vector(w, 400),
			size : new Vector(300, 87),
			depth : 1
		});
		
		soundSwitch = new Sprite( {
			name : 'soundSwitch',			
			texture : Luxe.loadTexture("assets/soundswitch.png"),
			pos : new Vector(50, h - 50),
			size : new Vector(60, 64),
			scale : new Vector(0,0),
			depth : 1
		});			
		
		exitButton = new Sprite( {
			name : 'exitButton',			
			texture : Luxe.loadTexture("assets/exitbutton.png"),
			pos : new Vector(130, h - 50),
			size : new Vector(60, 64),
			scale : new Vector(0,0),
			depth : 1,
		});			
		
		bestTemp = new Sprite( {
			name : 'bestTemp',
			centered : false,
			texture : Luxe.loadTexture("assets/best.png"),
			pos : new Vector(w / 2 - 100, h),
			size : new Vector(200, 65),
			depth : 1
		});
		
		border = new Body(BodyType.STATIC);
		border.shapes.add(new Polygon(Polygon.rect(0, 0, w, 5)));		
		border.space = Luxe.physics.nape.space;								
		
		newBox('b1', 60, 60, "assets/boxK.png", 20);
		newBox('b2', 140, 60, "assets/boxU.png", 80);
		newBox('b3',210, 60, "assets/boxB.png", 20);
		newBox('b4',290, 60, "assets/boxI.png", 80);
		newBox('b5',360, 60, "assets/boxK.png", 20);
		newBox('b6',420, 60, "assets/boxI.png", 80);		
		
		Luxe.audio.on('menumusic', 'load', function(_){
            Luxe.audio.loop('menumusic');
        });			
		
		Luxe.audio.on('slide', 'load', function(_) {
			Timer.delay(1, function() {
				Luxe.audio.play('slide');
				Actuate.tween(startButton.pos, 1, { x: Luxe.screen.w / 2 - startButton.size.x / 2 } );
			});
			
			Timer.delay(2, function() {
				Luxe.audio.play('slide');
				Actuate.tween(scoreButton.pos, 1, { x: Luxe.screen.w / 2 - scoreButton.size.x / 2 } );
			});
			
			Timer.delay(3, function() {
				Luxe.audio.play('slide');
				Actuate.tween(bestTemp.pos, 1, { y: 530 } );				   
			});
			
			Timer.delay(4, function() {
				Luxe.audio.play('slide');
				Actuate.tween(soundSwitch.scale, 1, { x: 1, y: 1 } );
			});
			
			Timer.delay(4.3, function() {
				Luxe.audio.play('slide');
				Actuate.tween(exitButton.scale, 1, { x: 1, y: 1 } ).onComplete(function() {
					isReady = true;
				});
			});
		});		
	}
	
	override public function onleave<T>(d:T)
	{		
		Luxe.audio.on('click', 'load', function(_) {						
			for (b in boxes) {
				b.destroyBox();
			}					
			
			Luxe.audio.play('click');
			Luxe.audio.stop('menumusic');
			
			Timer.delay(0.2, function() {
				Actuate.tween(startButton.pos, 0.5, { x: -startButton.size.x } );
				Actuate.tween(scoreButton.pos, 0.5, { x: Luxe.screen.w } );
				Actuate.tween(bestTemp.pos, 0.5, { y: Luxe.screen.h } );
				Actuate.tween(soundSwitch.scale, 0.5, { x : 0, y : 0 } );
				Actuate.tween(exitButton.scale, 0.5, { x : 0, y : 0 } ).onComplete(function() {
					Luxe.scene.remove(menuBack);		
					menuBack.destroy();					
				});
			});
		});
	}
	
	override public function onmouseup(e:MouseEvent) 
	{
		if (isReady) {		
			if (startButton.point_inside(e.pos)) {
				Main.states.set('game');
			} else if (scoreButton.point_inside(e.pos)) {
				Main.states.set('scores');
			}
		}
	}
	
	override public function update(dt:Float)
	{
		/*for (b in boxes) {
			b.update(dt);
		}*/		
	}
	
}